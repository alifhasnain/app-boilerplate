package com.niloy.appboilerplate.features

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MainViewModelFactory(val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val dao = MyDB.getInstance(context).myDao()
        return MainViewModel(dao) as T
    }

}