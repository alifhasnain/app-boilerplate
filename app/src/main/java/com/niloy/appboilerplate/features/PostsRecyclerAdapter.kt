package com.niloy.appboilerplate.features

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.niloy.appboilerplate.common.model.Post
import com.niloy.appboilerplate.databinding.ListItemPostsBinding

class PostsRecyclerAdapter: RecyclerView.Adapter<PostsRecyclerAdapter.ViewHolder>() {

    private val items: MutableList<Post> = mutableListOf()

    fun submitItems(updatedItems: List<Post>) {
        items.clear()
        items.addAll(updatedItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemPostsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(private val binding: ListItemPostsBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Post) {
            binding.postId.text = data.postID.toString()
            binding.userId.text = data.userID.toString()
            binding.title.text = data.title
            binding.body.text = data.body
        }
    }
}