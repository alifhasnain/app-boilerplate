package com.niloy.appboilerplate.features

sealed class LoadStates {
    object Loaded : LoadStates()
    object Empty: LoadStates()
    object Loading: LoadStates()
    class Error(val message: String): LoadStates()
}