package com.niloy.appboilerplate.features

import com.niloy.appboilerplate.common.model.Post
import retrofit2.http.GET

interface RetrofitAPI {

    @GET("posts")
    suspend fun getPosts(): List<Post>

}