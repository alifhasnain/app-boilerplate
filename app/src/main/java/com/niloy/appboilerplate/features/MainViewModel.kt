package com.niloy.appboilerplate.features

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.niloy.appboilerplate.common.model.Post
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.lang.Exception

class MainViewModel(val dao: MyDao): ViewModel() {

    private val retrofitAPI by lazy { RetrofitClient.INSTANCE }

    val postList: LiveData<List<Post>> = dao.getPosts()

    init {
        fetchPosts()
    }

    fun fetchPosts() = safeScope {
        val postList = retrofitAPI.getPosts()
        dao.insertPosts(postList)
    }

    private fun safeScope(block: (suspend () -> Unit)) = viewModelScope.launch {
        try {
            block()
        } catch (ex: HttpException) {
            Log.e("TAG",ex.message , ex)
        } catch (e: Exception) {
            Log.e("", "", e)
        }
    }
}