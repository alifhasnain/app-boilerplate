package com.niloy.appboilerplate.features

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.niloy.appboilerplate.common.model.Post

@Database(entities = [Post::class], version = 1)
abstract class MyDB: RoomDatabase() {

    abstract fun myDao(): MyDao

    companion object {

        @Volatile private var db: MyDB? = null

        fun getInstance(context: Context): MyDB = db ?: synchronized(this) {
            db ?: Room.databaseBuilder(context, MyDB::class.java, "mydb").fallbackToDestructiveMigration().build().also {
                db = it
            }
        }

    }
}