package com.niloy.appboilerplate.features

import okhttp3.OkHttp
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitClient {

    val INSTANCE: RetrofitAPI by lazy {
        val okHttpClient = OkHttpClient.Builder().apply {
            addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        }.build()
        Retrofit.Builder().apply {
            baseUrl("https://jsonplaceholder.typicode.com/")
            addConverterFactory(MoshiConverterFactory.create())
            client(okHttpClient)
        }.build().create(RetrofitAPI::class.java)
    }

}