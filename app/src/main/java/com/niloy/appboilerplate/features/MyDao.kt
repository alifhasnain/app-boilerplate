package com.niloy.appboilerplate.features

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.niloy.appboilerplate.common.model.Post

@Dao
interface MyDao {

    @Insert
    suspend fun insertPosts(postsList: List<Post>)

    @Query("select * from post")
    fun getPosts(): LiveData<List<Post>>

}