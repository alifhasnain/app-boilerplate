package com.niloy.appboilerplate.features

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.niloy.appboilerplate.R
import com.niloy.appboilerplate.databinding.ActivityMainBinding
import com.wada811.databinding.dataBinding
import com.wada811.viewbinding.viewBinding

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val viewModel: MainViewModel by viewModels { MainViewModelFactory(applicationContext) }

    private val binding: ActivityMainBinding by dataBinding()

    private val adapter: PostsRecyclerAdapter by lazy { PostsRecyclerAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        binding.recyclerView.adapter = adapter

        initializeObservers()

    }

    private fun initializeObservers() {
        viewModel.postList.observe(this) { posts ->
            adapter.submitItems(posts)
        }
    }
}