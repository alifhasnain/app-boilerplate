package com.niloy.appboilerplate.common.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity
data class Post(

    @field:Json(name = "userId") val userID: Long,

    @PrimaryKey
    @field:Json(name = "id")
    val postID: Long,

    val title: String,

    val body: String,
)